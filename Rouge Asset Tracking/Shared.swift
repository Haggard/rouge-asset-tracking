//
//  Shared.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-05.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import Foundation
import AVFoundation



final class Shared{
    static let shared = Shared()
    var sound: AVAudioPlayer?
    
    
    var timeOutTime = 25;
    
    var itemCurrentScanned = "";
    
    var flightNumber = "";
    var finNumber = "";
    var finToSave = "";
    var finLoadType = "";
    
    var finPossibleLocations = [Any]()
    
    var scannedCarrier = "";
    var scannedCarrierStartTime = Double();
    var scannedCarrierArray = [String:Double]()
    
    var scannedViewCarrier = "";
    var scannedViewCarrierStartTime = Double();
    var scannedViewCarrierArray = [String:Double]()
    
    var scannedVehicle = "";
    var scannedVehicleStartTime = Double()
    var scannedVehicleArray = [String:Double]()
    
    var scannedAircraft = "";
    var scannedAircraftStartTime = Double()
    var scannedAircraftArray = [String:Double]()
    
    var scannedUnloadAircraft = "";
    var scannedUnloadAircraftStartTime = Double()
    var scannedUnloadAircraftArray = [String:Double]()
    var scannedUnloadAircraftPosition = "";
    
    var scannedUnloadVehicle = "";
    var scannedUnloadVehicleStartTime = Double()
    var scannedUnloadVehicleArray = [String:Double]()
    
    var scannedUnloadCarrier = "";
    var scannedUnloadCarrierStartTime = Double();
    var scannedUnloadCarrierArray = [String:Double]()
    
    
    var scannedBlueBox = "";
    var scannedBlueBoxStartTime = Double();
    var scannedBlueBoxArray = [String:Double]()
    
    var scannedMoveIpad = "";
    var scannedMoveIpadStartTime = Double();
    var scannedMoveIpadArray = [String:Double]()
    
    var scannedFoundiPad = "";
    var scannedFoundiPadStartTime = Double();
    var scannedFoundiPadArray = [String:Double]()
    
    var scannedRevisitReason = ""
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func checkIfOnline() -> Bool{
        if Reachability.isConnectedToNetwork(){
            return true
        }
        else{
            return false
        }
    }
    
    
    func uploadStoredData(){
        let store = UserDefaults.standard
        
       
        
        
        
        
        if checkIfOnline() == true
        {
            //check if there is any data to upload!
            
            
            let iPadToCarrier = store.string(forKey: "iPadToCarrier")
            let CarrierToVehicle = store.string(forKey: "CarrierToVehicle")
            let CarrierToAircraft = store.string(forKey: "CarrierToAircraft")
            
            let CarrierFromAircraft = store.string(forKey: "CarrierFromAircraft")
            let CarrierFromVehicle = store.string(forKey: "CarrierFromVehicle")
            let iPadFromCarrier = store.string(forKey: "iPadFromCarrier")
            
            let revisit = store.string(forKey: "revisit")
            let cancelFlight = store.string(forKey: "cancelFlight")
            
            
            
            if(iPadToCarrier != "" && iPadToCarrier != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/load_carrier/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(iPadToCarrier!)
                
                let tester: Data? = iPadToCarrier?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "iPadToCarrier")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
                
            }
            if(CarrierToVehicle != "" && CarrierToVehicle != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/load_vehicle/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(CarrierToVehicle!)
                
                let tester: Data? = CarrierToVehicle?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "CarrierToVehicle")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
            }
            if(CarrierToAircraft != "" && CarrierToAircraft != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/load_aircraft/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(CarrierToAircraft!)
                
                let tester: Data? = CarrierToAircraft?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "CarrierToAircraft")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
            }
            
            
            if(CarrierFromAircraft != "" && CarrierFromAircraft != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/unload_aircraft/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(CarrierFromAircraft!)
                
                let tester: Data? = CarrierFromAircraft?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "CarrierFromAircraft")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
            }
            if(CarrierFromVehicle != "" && CarrierFromVehicle != nil)
            {
                        //Stuff to upload
                        let url = URL(string: "https://tracking.bulletdigital.com/unload_vehicle/?c668977d4ff12f105b1a5cccc1bb1b82")!
                        var request = URLRequest(url: url)
                        request.httpMethod = "POST"
                        
                        print(CarrierFromVehicle!)
                        
                        let tester: Data? = CarrierFromVehicle?.data(using: .utf8)
                        
                        request.httpBody = tester
                        
                        let task = URLSession.shared.dataTask(with: request) { data, response, error in
                            guard let data = data, error == nil else {
                                print(error?.localizedDescription ?? "No data")
                                return
                            }
                            print(data)
                            
                            let callback = String(decoding: data, as: UTF8.self)
                            
                            if(callback.contains("200"))
                            {
                                let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                                let url = URL(fileURLWithPath: path)
                                
                                do {
                                    self.sound = try AVAudioPlayer(contentsOf: url)
                                    self.sound?.play()
                                    
                                } catch {
                                    print("File not found!")
                                }
                                
                                //Remove all UserDefaults.
                                let store = UserDefaults.standard
                                store.set("", forKey: "CarrierFromVehicle")
                            }
                            else{
                                
                            }
                        }
                        task.resume()
                        
                    }
            if(iPadFromCarrier != "" && iPadFromCarrier != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/unload_carrier_force/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(iPadFromCarrier!)
                
                let tester: Data? = iPadFromCarrier?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "iPadFromCarrier")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
                
            }
            if(revisit != "" && revisit != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/revisit/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(revisit!)
                
                let tester: Data? = revisit?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "revisit")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
                
            }
            if(cancelFlight != "" && cancelFlight != nil)
            {
                //Stuff to upload
                let url = URL(string: "https://tracking.bulletdigital.com/canceled_flight/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                print(cancelFlight!)
                
                let tester: Data? = cancelFlight?.data(using: .utf8)
                
                request.httpBody = tester
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    
                    let callback = String(decoding: data, as: UTF8.self)
                    
                    if(callback.contains("200"))
                    {
                        let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            self.sound = try AVAudioPlayer(contentsOf: url)
                            self.sound?.play()
                            
                        } catch {
                            print("File not found!")
                        }
                        
                        //Remove all UserDefaults.
                        let store = UserDefaults.standard
                        store.set("", forKey: "cancelFlight")
                    }
                    else{
                        
                    }
                }
                task.resume()
                
                
            }
        }
    }
}
