//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation


class loadAircraft: UIViewController {
    
    @IBOutlet weak var saveBtn: UIButton!
    
    
    @IBOutlet weak var flashLight: UIButton!
       @IBAction func flash(_ sender: Any) {
           
           guard let device = AVCaptureDevice.default(for: .video) else { return }

           if device.hasTorch {
               do {
                   try device.lockForConfiguration()

                   if device.torchMode == .on {
                       toggleTorch(on: false)
                   } else {
                       toggleTorch(on: true)
                   }

                   device.unlockForConfiguration()
               } catch {
                   print("Torch could not be used")
               }
           } else {
               print("Torch is not available")
           }
           
           
       }
    
    
    @IBOutlet weak var topbar: UIView!
    
    @IBOutlet weak var messageButtonLabel: UIButton!
    
    @IBOutlet weak var airCraftInfo: UIButton!
    
    
    var scannedArray = [String:Double]()
    var scannedAircraft = "";
    
    var activeFin = ""
    
    var sound: AVAudioPlayer?
    
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    
    
    func showPopOver(){
        
    }
    
    
    @IBAction func UpdateFlightNumber(_ sender: Any) {
        let alertController = UIAlertController(title: "Update Flight Number", message: "", preferredStyle: .alert)
               alertController.addTextField { (textField : UITextField!) -> Void in
                textField.text = Shared.shared.flightNumber
                   textField.keyboardType = .asciiCapableNumberPad
                   
                   
               }
               
               let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
                   if let textField = alertController.textFields?[0] {
                       if textField.text!.count > 0 {
                           print("Text :: \(textField.text ?? "")")
                           Shared.shared.flightNumber = textField.text!
                        self.airCraftInfo.setTitle("Fin:\(self.activeFin), Flight: \(Shared.shared.flightNumber) Type:\(self.singleOrMulti)", for: .normal)
                       }
                   }
               })
               
               let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                   (action : UIAlertAction!) -> Void in })
               
               alertController.addAction(cancelAction)
               alertController.addAction(saveAction)
               
               alertController.preferredAction = saveAction
               
               
               
               DispatchQueue.main.async {
                   self.present(alertController, animated: true, completion: nil)
               }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if(Shared.shared.scannedAircraft.count == 0)
        {
            if(Shared.shared.scannedAircraft == "")
            {
                messageButtonLabel.setTitle("Scan The Position You Want To Load", for: .normal)
            }
            else
            {
                messageButtonLabel.setTitle("\(Shared.shared.scannedAircraft ) Ready To Load", for: .normal)
            }
            
        }
        else
        {
            messageButtonLabel.setTitle("\(Shared.shared.scannedAircraft ) has: \(Shared.shared.scannedAircraftArray.count) loaded", for: .normal)
        }
        Shared.shared.uploadStoredData()
        
        
        if let date = UserDefaults.standard.object(forKey: "activeTime") as? Date {
            if let diff = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, diff > Shared.shared.timeOutTime {
                // Log the User out!
                DispatchQueue.main.async {
                    UserDefaults.standard.set(nil, forKey: "activeTime")
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
                let store = UserDefaults.standard
                let timeToStore = Date()
                store.set(timeToStore, forKey: "activeTime")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("I am disapearing look at me!")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
    
    var flightNumber = "";
    var singleOrMulti = "";
    
    
    func singOrMul (){
        let alertController2 = UIAlertController(title: "Single Or Multi Carrier Loading?", message: "", preferredStyle: .alert)
        
        
        let single = UIAlertAction(title: "Single", style: .default, handler: { alert -> Void in
            self.singleOrMulti = "Single"
            print(self.singleOrMulti)
            self.airCraftInfo.setTitle("Flight: \(Shared.shared.flightNumber) Type:\(self.singleOrMulti)", for: .normal)
        })
        
        let multi = UIAlertAction(title: "Multi", style: .default, handler: { alert -> Void in
            self.singleOrMulti = "Multi"
            print(self.singleOrMulti)
            self.airCraftInfo.setTitle("Flight: \(Shared.shared.flightNumber) Type:\(self.singleOrMulti)", for: .normal)
        })
        
        
        
        alertController2.addAction(single)
        alertController2.addAction(multi)
        
        alertController2.preferredAction = multi
        
        
        DispatchQueue.main.async {
            self.present(alertController2, animated: true, completion: nil)
        }
    }
    
    func getFlight(){
        let alertController = UIAlertController(title: "Enter Flight Number", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Flight Number"
            textField.keyboardType = .asciiCapableNumberPad
            
            
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    Shared.shared.flightNumber = textField.text!
                   
                    self.singOrMul()
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in Shared.shared.flightNumber = "N/A"
          
            self.singOrMul()
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Shared.shared.resetDefaults()
        
        
        getFlight()
        
        
        
        
        messageButtonLabel.setTitle("Scan The Position You Want To Load", for: .normal)
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self as AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
        } catch {
            print(error)
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        captureSession.startRunning()
        
        
        view.bringSubviewToFront(messageButtonLabel)
        view.bringSubviewToFront(topbar)
        view.bringSubviewToFront(airCraftInfo)
        view.bringSubviewToFront(flashLight)
        
        
        
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        let alert = UIAlertController(title: "Notice", message: "Are you sure you want to cancel your current scan? Doing so will remove all active scans.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done, take me back home", style: UIAlertAction.Style.default, handler: { action in
            self.cancelScan()
        }))
        alert.addAction(UIAlertAction(title: "OOPS! Didn't mean to do that", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelScan(){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    @IBAction func save(_ sender: Any) {
        let items = Shared.shared.scannedAircraftArray;
        let Aircraft = Shared.shared.scannedAircraft;
        
        if(items.count == 0 || Aircraft == "")
        {
            let alert = UIAlertController(title: "Scan Position or Carrier", message: "You need to load a Aircraft before you can save it. Please scan a Position and then scan the Carrier as you load them.", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let store = UserDefaults.standard
            let uuid = store.string(forKey: "uuid")
            
            
            
            
            
            
            
            let stuffToSend = [
                Shared.shared.finToSave: [
                    "UUID":uuid!,
                    "Carriers":items,
                    "TimeSaved": Date().timeIntervalSince1970,
                    "AircraftTimeStarted": Shared.shared.scannedAircraftStartTime,
                    "Type":singleOrMulti,
                    "FlightNumber":Shared.shared.flightNumber
                    ] as [String:Any],
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
            
            
            if Reachability.isConnectedToNetwork(){
                let url = URL(string: "https://tracking.bulletdigital.com/load_aircraft/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        print(responseJSON)
                        
                        let test = responseJSON["response_code"]
                        if(test as! Int == 200)
                        {
                            let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                
                                self.sound = try AVAudioPlayer(contentsOf: url)
                                self.sound?.play()
                            } catch {
                                print("File not found!")
                            }
                            
                            //Clear out of the list
                            Shared.shared.scannedAircraftArray.removeAll()
                            Shared.shared.scannedAircraft = ""
                            
                            DispatchQueue.main.async {
                                self.messageButtonLabel.setTitle("Scan The Position You Want To Load", for: .normal)
                            }
                            
                        }
                    }
                }
                task.resume()
            }else{
                print("Internet Connection not Available!")
                print("User is offline so lets save that data somewhere to send later!")
                
                //CHECK IF THERE IS ANYTHING IN AircraftToAircraft  USER DEFUALT STORAGE
                let store = UserDefaults.standard
                let CarrierToAircraft = store.string(forKey: "CarrierToAircraft")
                
                if(CarrierToAircraft != nil && CarrierToAircraft != "" && CarrierToAircraft != "{:}"){
                    let data = Data(CarrierToAircraft!.utf8)
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            print(json)
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    
                    // KEEP CarrierToAircraft as the main, remove the first and last,
                    let modIpad = CarrierToAircraft?.dropLast(1)
                    let modScanned = jsonString?.dropFirst(1)
                    let jsonString123 = modIpad! + "," + modScanned!
                    print(jsonString123)
                    store.set(jsonString123,forKey: "CarrierToAircraft")
                    
                    let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                    let url = URL(fileURLWithPath: path)
                    do {
                        self.sound = try AVAudioPlayer(contentsOf: url)
                        self.sound?.play()
                    } catch {
                        // couldn't load file :(
                        print("File not found!")
                    }
                    
                    Shared.shared.scannedAircraft = ""
                    Shared.shared.scannedAircraftArray.removeAll()
                    messageButtonLabel.setTitle("Scan The Position You Want To Load", for: .normal)
                }
                else{
                    //FALSE--- ADD THE NEW STUFF TO USER DEFAULT STORAGE
                    let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
                    let jsonString = String(data: jsonData!, encoding: .utf8)
                    
                    store.set(jsonString,forKey: "CarrierToAircraft")
                    
                    Shared.shared.scannedAircraft = ""
                    Shared.shared.scannedAircraftArray.removeAll()
                    messageButtonLabel.setTitle("Scan The Position You Want To Load", for: .normal)
                }
                self.saveBtn.blink(enabled:false)
                self.saveBtn.backgroundColor = UIColor.clear
                return
            }
            print("User is online so lets save that data!")
        }
    }
}

extension loadAircraft: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let code = metadataObj.stringValue
                
                if code!.contains("AR"){
                    print("Aircraft Scanned");
                    
                    if Shared.shared.scannedAircraft == code!{
                        
                    }
                    else if( Shared.shared.scannedAircraft != "")
                    {
                        
                        if singleOrMulti == "Single"
                        {
                            let path = Bundle.main.path(forResource: "error.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                sound = try AVAudioPlayer(contentsOf: url)
                                sound?.play()
                            } catch {
                                // couldn't load file :(
                                print("File not found!")
                            }
                            
                            
                            //Show alert saying that a Aircraft has already been scanned
                            let alert = UIAlertController(title: "Duplicate Position", message: "You have already scanned a Position with the tag: \(Shared.shared.scannedAircraft). Starting a new Position will erease the Carrier scanned to the old Position", preferredStyle: UIAlertController.Style.alert)
                            
                            // add the actions (buttons)
                            alert.addAction(UIAlertAction(title: "Start Loading The New \(code!) Position", style: UIAlertAction.Style.default, handler: { action in
                                //print("Fuck, now I have to handle if they want to load a new Aircraft")
                                
                                //Clear out of the list
                                //  Shared.shared.dataToShow.removeAll()
                                Shared.shared.scannedAircraft = code!
                                Shared.shared.scannedAircraftStartTime = Date().timeIntervalSince1970;
                                Shared.shared.scannedAircraftArray.removeAll()
                                
                                DispatchQueue.main.async {
                                    //messageButtonLabel.setTitle("\(Shared.shared.scannedAircraft ) Ready To Load", for: .normal)
                                    self.messageButtonLabel.setTitle("\(code! ) Ready To Load", for: .normal)
                                }
                                
                                //Wipe out eve
                            }))
                            alert.addAction(UIAlertAction(title: "OOPS! Keep loading current Position", style: UIAlertAction.Style.cancel, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if singleOrMulti == "Multi"
                        {
                            var count = Shared.shared.scannedAircraftArray.count
                            if ( count == 0)
                            {
                                Shared.shared.scannedAircraft  = code!
                                //First item being scanned
                                print("hit First")
                                let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                                let url = URL(fileURLWithPath: path)
                                
                                do {
                                    sound = try AVAudioPlayer(contentsOf: url)
                                    sound?.play()
                                } catch {
                                    // couldn't load file :(
                                    print("File not found!")
                                }
                                
                            }
                            else{
                                //Multi
                                Shared.shared.scannedAircraft  = code!
                                print("hit Multi!")
                                let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                                let url = URL(fileURLWithPath: path)
                                
                                do {
                                    sound = try AVAudioPlayer(contentsOf: url)
                                    sound?.play()
                                } catch {
                                    // couldn't load file :(
                                    print("File not found!")
                                }
                            }
                        }
                        
                        
                        
                        
                    }
                    else
                    {
                        //Scanned Carrier is blank.
                        //Update scannedAircraft value to the one scanned.
                        Shared.shared.itemCurrentScanned = "Aircraft"
                        Shared.shared.scannedAircraft  = code!
                        Shared.shared.scannedAircraftStartTime = Date().timeIntervalSince1970;
                        
                        let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            sound = try AVAudioPlayer(contentsOf: url)
                            sound?.play()
                        } catch {
                            // couldn't load file :(
                            print("File not found!")
                        }
                        
                        loadFinInfo()
                        
                        
                        
                    }
                }
                else if code!.contains("CA"){
                    if Shared.shared.scannedAircraft  != ""
                    {
                        //Carrier has a value. Start loading iPads into the carrier.
                        
                        // let keyExsists = scannedArray[code!] != nil
                        
                        //if Shared.shared.dataToShow.contains(code!)
                        var shit = code! + "-\(Shared.shared.scannedAircraft)"
                        let keyExsists = Shared.shared.scannedAircraftArray[shit] != nil
                        if keyExsists
                        {
                            
                        }
                        else{
                            //Shared.shared.dataToShow.append(code!)
                            var item = code! + "-\(Shared.shared.scannedAircraft )"
                            Shared.shared.scannedAircraftArray[item] = Date().timeIntervalSince1970
                            
                            
                            let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                sound = try AVAudioPlayer(contentsOf: url)
                                sound?.play()
                            } catch {
                                // couldn't load file :(
                                print("File not found!")
                            }
                            self.saveBtn.backgroundColor = UIColor.green
                                                       self.saveBtn.blink()
                            messageButtonLabel.setTitle("\(Shared.shared.flightNumber ) has: \(Shared.shared.scannedAircraftArray.count) loaded", for: .normal)
                            
                        }
                        
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Scan Position", message: "You need to scan a Position before you can scan an Carrier", preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else if code!.contains("SP"){
                    let alert = UIAlertController(title: "Scan a Position or Carrier", message: "Please Scan a Position or Carrier", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("VE"){
                    let alert = UIAlertController(title: "Scan a Position or Carrier", message: "Please Scan a Position or Carrier", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
   
    
    func loadFinInfo(){
        var AR = Shared.shared.scannedAircraft
        
        let store = UserDefaults.standard
        let fins = store.string(forKey: "fins")
        
        let data = Data(fins!.utf8)
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                print(json)
                
                var value = json[AR]
                
                
                if(value != nil)
                {
                    print(value!)
                    
                    
                    activeFin = value as! String
                    Shared.shared.finToSave = value as! String
                    
                    airCraftInfo.setTitle("Fin:\(activeFin), Flight: \(Shared.shared.flightNumber) Type:\(singleOrMulti)", for: .normal)
                    messageButtonLabel.setTitle("\(Shared.shared.scannedAircraft ) Ready To Load", for: .normal)
                }
                else{
                    activeFin = "N/A"
                    Shared.shared.finToSave = "N/A"
                    
                    airCraftInfo.setTitle("Fin:\(activeFin), Flight: \(Shared.shared.flightNumber) Type:\(singleOrMulti)", for: .normal)
                    messageButtonLabel.setTitle("\(Shared.shared.scannedAircraft ) Ready To Load", for: .normal)
                }
                
                
                
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
        
    }
    
    func offlineUpload(){
        //Check if CarrierToAircraft has values
        
    }
    
    
    
    func toggleTorch(on: Bool) {
           guard let device = AVCaptureDevice.default(for: .video) else { return }

           if device.hasTorch {
               do {
                   try device.lockForConfiguration()

                   if on == true {
                       device.torchMode = .on
                   } else {
                       device.torchMode = .off
                   }

                   device.unlockForConfiguration()
               } catch {
                   print("Torch could not be used")
               }
           } else {
               print("Torch is not available")
           }
       }
}
