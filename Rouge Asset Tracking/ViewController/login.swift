//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit

class login: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    func loadUsers(){
        if Reachability.isConnectedToNetwork(){
                   //Check is USer
                   let store = UserDefaults.standard
                   let users = store.string(forKey: "users")
                   
                   let url = URL(string: "https://tracking.bulletdigital.com/users/?c668977d4ff12f105b1a5cccc1bb1b82")!
                   var request = URLRequest(url: url)
                   request.httpMethod = "POST"
                   
                   
                   let task = URLSession.shared.dataTask(with: request) { data, response, error in
                       guard let data = data, error == nil else {
                           print(error?.localizedDescription ?? "No data")
                           return
                       }
                       print(data)
                       let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                       if let responseJSON = responseJSON as? [String: Any] {
                           print(responseJSON)
                           
                           let test = responseJSON["response_code"]
                           if(test as! Int == 200)
                           {
                               if users == responseJSON["data"] as? String
                               {
                                   
                               }
                               else
                               {
                                   let store = UserDefaults.standard
                                   store.set(responseJSON["data"], forKey: "users")
                               }
                           }
                           else{
                               
                           }
                       }
                   }
                   task.resume()
               }
    }
    
    
      func loadFins(){
          if Reachability.isConnectedToNetwork(){
                     //Check is USer
                     let store = UserDefaults.standard
                     let fins = store.string(forKey: "fins")
                     
                     let url = URL(string: "https://tracking.bulletdigital.com/get_fins/?c668977d4ff12f105b1a5cccc1bb1b82")!
                     var request = URLRequest(url: url)
                     request.httpMethod = "POST"
                     
                     
                     let task = URLSession.shared.dataTask(with: request) { data, response, error in
                         guard let data = data, error == nil else {
                             print(error?.localizedDescription ?? "No data")
                             return
                         }
                         print(data)
                         let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                         if let responseJSON = responseJSON as? [String: Any] {
                             print(responseJSON)
                             
                             let test = responseJSON["response_code"]
                             if(test as! Int == 200)
                             {
                                 if fins == responseJSON["data"] as? String
                                 {
                                     
                                 }
                                 else
                                 {
                                     let store = UserDefaults.standard
                                     store.set(responseJSON["data"], forKey: "fins")
                                 }
                             }
                             else{
                                 
                             }
                         }
                     }
                     task.resume()
                 }
      }
    
    func loadFinsFix(){
             if Reachability.isConnectedToNetwork(){
                        //Check is USer
                        let store = UserDefaults.standard
                        let fins = store.string(forKey: "finsFix")
                        
                        let url = URL(string: "https://tracking.bulletdigital.com/get_fins_fix/?c668977d4ff12f105b1a5cccc1bb1b82")!
                        var request = URLRequest(url: url)
                        request.httpMethod = "POST"
                        
                        
                        let task = URLSession.shared.dataTask(with: request) { data, response, error in
                            guard let data = data, error == nil else {
                                print(error?.localizedDescription ?? "No data")
                                return
                            }
                            print(data)
                            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                            if let responseJSON = responseJSON as? [String: Any] {
                                print(responseJSON)
                                
                                let test = responseJSON["response_code"]
                                if(test as! Int == 200)
                                {
                                    if fins == responseJSON["data"] as? String
                                    {
                                        
                                    }
                                    else
                                    {
                                        let store = UserDefaults.standard
                                        store.set(responseJSON["data"], forKey: "finsFix")
                                    }
                                }
                                else{
                                    
                                }
                            }
                        }
                        task.resume()
                    }
         }
    
    override func viewWillAppear(_ animated: Bool) {
       
        loadUsers()
        loadFins()
        loadFinsFix()
        Shared.shared.uploadStoredData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboard(notification:)), name:UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboard(notification:Notification) {
        guard let keyboardReact = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{
            return
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification ||  notification.name == UIResponder.keyboardWillChangeFrameNotification {
            self.view.frame.origin.y = -keyboardReact.height
        }else{
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func login(_ sender: Any) {
        let username = userName.text
        let code = password.text
        
        if(username != "" && code != "")
        {
            let stuffToSend = ["username":username!, "code":code!] as [String:Any]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
       /*
            if Shared.shared.checkIfOnline() == true
            {
                print("Internet Connection Available!")
                
                // create post request
                let url = URL(string: "https://tracking.bulletdigital.com/login/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                // insert json data to the request
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        print(responseJSON)
                        
                        let test = responseJSON["response_code"]
                        if(test as! Int == 200)
                        {
                            let store = UserDefaults.standard
                            store.set(responseJSON["uuid"], forKey: "uuid")
                            DispatchQueue.main.async {
                                self.goToHome()
                            }
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Error", message: "user not found, please try again", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { action in }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
                
                task.resume()
            }
            else
            {
     */           let store = UserDefaults.standard
                let users = store.string(forKey: "users")
                
                let data = Data(users!.utf8)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        if case let val as Dictionary<String, Any> = json[username!] {
                            let codeTest = val["code"]
                            let uuid = val["uuid"]
                            
                            if codeTest as! String == code!
                            {
                                let store = UserDefaults.standard
                                store.set(uuid!, forKey: "uuid")
                                DispatchQueue.main.async {
                                    self.goToHome()
                                }
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Error", message: "Username or password not found.", preferredStyle: UIAlertController.Style.alert)
                                
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { action in
                                    return
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Error", message: "Username or password not found.", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { action in
                                return
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
           // }
        }
        else{
            let alert = UIAlertController(title: "Error", message: "Fill out username and password", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { action in
                return
            }))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func goToHome(){
        
        let store = UserDefaults.standard
        let timeToStore = Date()
        store.set(timeToStore, forKey: "activeTime")
        
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}

