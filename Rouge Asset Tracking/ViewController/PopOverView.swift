//
//  PopOverView.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-05.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import Foundation
import UIKit


class PopOverView: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate{
    @IBOutlet weak var Popupview: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var name: [String] = Array();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Shared.shared.itemCurrentScanned == "Vehicle"
        {
            for(key, _) in Shared.shared.scannedVehicleArray
            {
                name.append(key)
            }
        }
            
        else if Shared.shared.itemCurrentScanned == "Carrier"
        {
            for(key, _) in Shared.shared.scannedCarrierArray
            {
                name.append(key)
            }
        }
            
        else if Shared.shared.itemCurrentScanned == "Aircraft"
        {
            for(key, _) in Shared.shared.scannedAircraftArray
            {
                name.append(key)
            }
        }
        else if Shared.shared.itemCurrentScanned == "UnloadVehicle"
        {
            for(key, _) in Shared.shared.scannedUnloadVehicleArray
            {
                name.append(key)
            }
        }
            
        else if Shared.shared.itemCurrentScanned == "UnloadCarrier"
        {
            for(key, _) in Shared.shared.scannedUnloadCarrierArray
            {
                name.append(key)
            }
        }
            
        else if Shared.shared.itemCurrentScanned == "UnloadAircraft"
        {
            for(key, _) in Shared.shared.scannedUnloadAircraftArray
            {
                name.append(key)
            }
        }
        else if Shared.shared.itemCurrentScanned == "BlueBox"
        {
            for(key, _) in Shared.shared.scannedBlueBoxArray
            {
                name.append(key)
            }
        }
        else if Shared.shared.itemCurrentScanned == "Found"
        {
            for(key, _) in Shared.shared.scannedFoundiPadArray
            {
                name.append(key)
            }
        }
        else if Shared.shared.itemCurrentScanned == "ViewCarrier"
        {
            for(key, _) in Shared.shared.scannedViewCarrierArray
            {
                name.append(key)
            }
        }
        
        else if Shared.shared.itemCurrentScanned == "MoveIpad"
        {
            for(key, _) in Shared.shared.scannedMoveIpadArray
            {
                name.append(key)
            }
        }
        
        
        
        
        
        
        // Apply radius to Popupview
        Popupview.layer.cornerRadius = 10
        Popupview.layer.masksToBounds = true
    }
    
    
    // Returns count of items in tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count;
    }
    
    
    // Select item from tableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    //Assign values for tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = name[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            if Shared.shared.itemCurrentScanned == "Vehicle"
            {
                Shared.shared.scannedVehicleArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "Carrier"
            {
                Shared.shared.scannedCarrierArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "Aircraft"
            {
                Shared.shared.scannedAircraftArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "UnloadVehicle"
            {
                Shared.shared.scannedUnloadVehicleArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "UnloadCarrier"
            {
                Shared.shared.scannedUnloadCarrierArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "UnloadAircraft"
            {
                Shared.shared.scannedUnloadAircraftArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "BlueBox"
            {
                Shared.shared.scannedBlueBoxArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "Found"
            {
                Shared.shared.scannedFoundiPadArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "ViewCarrier"
            {
                Shared.shared.scannedViewCarrierArray.removeValue(forKey: name[indexPath.row])
            }
            else if Shared.shared.itemCurrentScanned == "MoveIpad"
            {
                Shared.shared.scannedMoveIpadArray.removeValue(forKey: name[indexPath.row])
            }
            
            
          
            name.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            
            
        }
    }
    
    // Close PopUp
    @IBAction func closePopup(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.popoverPresentationController?.delegate = self
    }
}

