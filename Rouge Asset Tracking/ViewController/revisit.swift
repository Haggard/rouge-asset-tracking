//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation


class revisit: UIViewController {
    
    var sound: AVAudioPlayer?
    
    @IBOutlet weak var saveBtn: UIButton!
    
    var scannedCode = ""
    
    @IBOutlet weak var flashLight: UIButton!
    @IBAction func flash(_ sender: Any) {
        
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if device.torchMode == .on {
                    toggleTorch(on: false)
                } else {
                    toggleTorch(on: true)
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
        
        
    }
    
    
    @IBOutlet weak var topbar: UIView!
    
    @IBOutlet weak var messageButtonLabel: UIButton!
    
    @IBOutlet weak var airCraftInfo: UIButton!
    
    
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    
    
    func showPopOver(){
        
    }
    
    
    @IBAction func UpdateFlightNumber(_ sender: Any) {
        getFlightNum()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if(Shared.shared.scannedAircraftArray.count == 0)
        {
            scannedCode = ""
            messageButtonLabel.setTitle("\(Shared.shared.finNumber) Ready to load, Scan Carrier", for: .normal)
            self.saveBtn.blink(enabled:false)
            self.saveBtn.backgroundColor = UIColor.clear
        }
        else
        {
            messageButtonLabel.setTitle("\(Shared.shared.finNumber) has: \(Shared.shared.scannedAircraftArray.count) scan", for: .normal)
        }
        Shared.shared.uploadStoredData()
        
        
        if let date = UserDefaults.standard.object(forKey: "activeTime") as? Date {
            if let diff = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, diff > Shared.shared.timeOutTime {
                // Log the User out!
                DispatchQueue.main.async {
                    UserDefaults.standard.set(nil, forKey: "activeTime")
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
                let store = UserDefaults.standard
                let timeToStore = Date()
                store.set(timeToStore, forKey: "activeTime")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("I am disapearing look at me!")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
    
    
    func getFlightNum(){
        let alertController = UIAlertController(title: "Enter Flight Number", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            if(Shared.shared.flightNumber != "")
            {
                textField.text = Shared.shared.flightNumber
                textField.keyboardType = .asciiCapableNumberPad
            }
            else
            {
                textField.placeholder = "Flight Number"
                textField.keyboardType = .asciiCapableNumberPad
            }
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    Shared.shared.flightNumber = textField.text!
                    self.getFinNum()
                }
            }
        })
        
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func getFinNum(){
        let alertController = UIAlertController(title: "Enter FIN", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            if(Shared.shared.finNumber != "")
            {
                textField.text = Shared.shared.finNumber
                textField.keyboardType = .asciiCapableNumberPad
            }
            else
            {
                textField.placeholder = "Enter FIN"
                textField.keyboardType = .asciiCapableNumberPad
            }
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    Shared.shared.finNumber = textField.text!
                    self.getFinPos()
                }
            }
        })
        
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func popAlert(title: String, message: String, okAction: String, cancelAction:String, actionToDo:Int){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: okAction, style: UIAlertAction.Style.default, handler: { action in
            if actionToDo == 1
            {
                //Reload FIN
                self.getFinNum()
            }
        }))
        
        if actionToDo == 1
        {
            //alert.addAction(UIAlertAction(title: cancelAction, style: UIAlertAction.Style.cancel, handler: nil))
        }
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func getFinPos(){
        var scannedFin = Shared.shared.finNumber
        let store = UserDefaults.standard
        let fins = store.string(forKey: "finsFix")
        
        let data = Data(fins!.utf8)
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                print(json)
                var value = [Any]()
                if json[scannedFin] != nil
                {
                    value = json[scannedFin] as! [Any]
                    
                    Shared.shared.finPossibleLocations = value
                }
                else{
                    //fin not found alert
                    popAlert(title: "FIN NOT FOUND",message: "The Fin You Entered in not in the list, please try again.",okAction: "Okay",cancelAction: "Cancel", actionToDo:1)
                    return
                }
                
                if(value.count == 0)
                {
                    
                    
                }
                
                if(value.count > 1)
                {
                    //Multi load
                    print("mulit")
                    Shared.shared.finLoadType = "Multi"
                }
                else
                {
                    //Single load
                    print("Single")
                    Shared.shared.finLoadType = "Single"
                }
                
                //Update Btn text
                airCraftInfo.setTitle("FN: \(Shared.shared.finNumber) - FL: \(Shared.shared.flightNumber).", for: .normal)
                messageButtonLabel.setTitle("Scan Carrier", for: .normal)
                
                
                let alert = UIAlertController(title: "FIN Swap or Canceled Flight?", message: "Are you revisiting the aircraft for a FIN Swap or has the Flight been canceled.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "FIN SWAP", style: UIAlertAction.Style.default, handler: { action in
                    Shared.shared.scannedRevisitReason = "FIN Swap"
                }))
                alert.addAction(UIAlertAction(title: "Canceled Flight", style: UIAlertAction.Style.cancel, handler: {action in
                    Shared.shared.scannedRevisitReason = "Canceled Flight"
                }))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Get Flight #
        getFlightNum()
        //Get FIN Number
        
        print("Flight Number: \(Shared.shared.flightNumber)")
        print("FIN Number: \(Shared.shared.finNumber)")
        
        
        
        //Shared.shared.resetDefaults()
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self as AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
        } catch {
            print(error)
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        captureSession.startRunning()
        
        
        view.bringSubviewToFront(messageButtonLabel)
        view.bringSubviewToFront(topbar)
        view.bringSubviewToFront(airCraftInfo)
        view.bringSubviewToFront(flashLight)
        
        
        
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        let alert = UIAlertController(title: "Notice", message: "Are you sure you want to cancel your current scan? Doing so will remove all active scans.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done, take me back home", style: UIAlertAction.Style.default, handler: { action in
            Shared.shared.scannedAircraft = ""
                       Shared.shared.scannedAircraftArray.removeAll()
                       Shared.shared.finNumber = ""
                       Shared.shared.flightNumber = ""
            self.scannedCode = ""
                       Shared.shared.scannedRevisitReason = ""
                       self.saveBtn.blink(enabled:false)
                       self.saveBtn.backgroundColor = UIColor.clear
            self.cancelScan()
        }))
        alert.addAction(UIAlertAction(title: "OOPS! Didn't mean to do that", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelScan(){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    @IBAction func save(_ sender: Any) {
        let items = Shared.shared.scannedAircraftArray;
        // let Aircraft = Shared.shared.scannedAircraft;
        
        
        if(items.count == 0 )
        {
            let alert = UIAlertController(title: "Error", message: "You need to load a Aircraft before you can save it. Please scan a Carrier.", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let store = UserDefaults.standard
            let uuid = store.string(forKey: "uuid")
            
            
            
            
            
            
            
            let stuffToSend = [
                Shared.shared.finNumber: [
                    "UUID":uuid!,
                    "Carriers":items,
                    "TimeSaved": Date().timeIntervalSince1970,
                    "FlightNumber":Shared.shared.flightNumber,
                    "FIN":Shared.shared.finNumber,
                    "Reason":Shared.shared.scannedRevisitReason
                    ] as [String:Any],
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
            
            
            
            print("Internet Connection not Available!")
            print("User is offline so lets save that data somewhere to send later!")
            
            //CHECK IF THERE IS ANYTHING IN AircraftToAircraft  USER DEFUALT STORAGE
            
            let CarrierToAircraft = store.string(forKey: "revisit")
            
            if(CarrierToAircraft != nil && CarrierToAircraft != "" && CarrierToAircraft != "{:}"){
                let data = Data(CarrierToAircraft!.utf8)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
                // KEEP CarrierToAircraft as the main, remove the first and last,
                let modIpad = CarrierToAircraft?.dropLast(1)
                let modScanned = jsonString?.dropFirst(1)
                let jsonString123 = modIpad! + "," + modScanned!
                print(jsonString123)
                store.set(jsonString123,forKey: "revisit")
                
                let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                let url = URL(fileURLWithPath: path)
                do {
                    self.sound = try AVAudioPlayer(contentsOf: url)
                    self.sound?.play()
                } catch {
                    // couldn't load file :(
                    print("File not found!")
                }
                
                
            }
            else{
                //FALSE--- ADD THE NEW STUFF TO USER DEFAULT STORAGE
                let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                
                store.set(jsonString,forKey: "revisit")
                
                
            }
            Shared.shared.scannedAircraft = ""
            Shared.shared.scannedAircraftArray.removeAll()
            Shared.shared.finNumber = ""
            Shared.shared.flightNumber = ""
            scannedCode = ""
            Shared.shared.scannedRevisitReason = ""
            self.saveBtn.blink(enabled:false)
            self.saveBtn.backgroundColor = UIColor.clear
            self.cancelScan()
            return
        }
        print("User is online so lets save that data!")
        
        
        
        
        
    }
}

extension revisit: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let code = metadataObj.stringValue
                
                if code!.contains("AR"){
                    let alert = UIAlertController(title: "Scan Carrier", message: "Please Scan The Carrier You Want Are Revisiting", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("CA"){
                    print(Shared.shared.scannedAircraftArray)
                    var shit = Shared.shared.scannedAircraftArray
                    
                    if let entry = shit.first(where: { (key, _) in key.contains(code!) }) {
                        print(entry.value)
                        //Carrier already scanned!
                        //Pop Alert showing positions
                        let alert = UIAlertController(title: "Duplicate Carrier", message: "Carrier has already been scanned and assigned a position.", preferredStyle: UIAlertController.Style.alert)
                        
                        
                        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { action in
                            
                        }))
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                    let url = URL(fileURLWithPath: path)
                    
                    do {
                        sound = try AVAudioPlayer(contentsOf: url)
                        sound?.play()
                    } catch {
                        // couldn't load file :(
                        print("File not found!")
                    }
                    
                    
                    if code! == scannedCode
                    {
                        
                    }
                    else{
                        scannedCode = code!
                        
                        
                        
                        //Pop alert showing list of positions
                        var finLoadType = Shared.shared.finLoadType
                       // if finLoadType == "Multi"
                       // {
                            var pos = Shared.shared.finPossibleLocations as! [String]
                            var act = Shared.shared.scannedAircraftArray
                            
                            print(pos)
                            print(act)
                            
                            //Get list of Used Location(act) and See if they are in all locarions(pos)
                            
                            //Forech location in used compare to each of the ones in the master list and see if they compare
                            for loc in pos {
                                print("LOC: \(loc)")
                                for loc2 in act{
                                    print("LOC2: \(loc2)")
                                    
                                    
                                    print(loc)
                                    if let entry = act.first(where: { (key, _) in key.contains(loc as! String) }) {
                                        print(entry.value)
                                        //Remove the entry from the finPossibleLocations
                                        
                                        
                                        if let index = pos.index(of: loc) {
                                            pos.remove(at: index)
                                        }
                                        
                                        
                                    }
                                }
                            }
                            
                            
                            //Pop Alert showing positions
                            let alert = UIAlertController(title: "Select Location", message: "", preferredStyle: UIAlertController.Style.alert)
                            
                            //Foreach
                            for loc in pos {
                                alert.addAction(UIAlertAction(title: "\(loc)", style: UIAlertAction.Style.default, handler: { action in
                                    self.savePos(pos: loc, carrier: "\(code!)")
                                }))
                            }
                            alert.addAction(UIAlertAction(title: "Not In Position", style: UIAlertAction.Style.default, handler: { action in
                                self.savePos(pos: "NotInPosition", carrier: "\(code!)")
                            }))
                            
                            // Cancel button
                            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in }))
                            
                            self.present(alert, animated: true, completion: nil)
                       // }
                        //else {
                            
                       // }
                        return
                        
                        
                        
                        
                        
                        
                        
                        
                    }
                    
                }
                else if code!.contains("SP"){
                    let alert = UIAlertController(title: "Scan Carrier", message: "Please Scan The Carrier You Are Revisiting", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("VE"){
                    let alert = UIAlertController(title: "Scan Carrier", message: "Please Scan The Carrier You Are Revisiting", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func savePos(pos:Any, carrier:Any){
        print(pos)
        print(carrier)
        Shared.shared.itemCurrentScanned = "Aircraft"
        
        
        //update the scanned Array
        var shit = carrier as! String + "-\(pos as! String)"
        
        
        
        print(shit)
        let keyExsists = Shared.shared.scannedAircraftArray[shit] != nil
        if keyExsists
        {
            
        }
        else{
            //    var item = pos as! String + "-\(scannedCode)"
            Shared.shared.scannedAircraftArray[shit] = Date().timeIntervalSince1970
            
            
            let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
            let url = URL(fileURLWithPath: path)
            
            do {
                sound = try AVAudioPlayer(contentsOf: url)
                sound?.play()
            } catch {
                // couldn't load file :(
                print("File not found!")
            }
            self.saveBtn.backgroundColor = UIColor.green
            self.saveBtn.blink()
            messageButtonLabel.setTitle("\(Shared.shared.finNumber) has: \(Shared.shared.scannedAircraftArray.count) scan", for: .normal)
            
        }
        
        
    }
    
    
    
    
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
}
