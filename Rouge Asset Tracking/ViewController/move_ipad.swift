//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation


class move_ipad: UIViewController {
    
    @IBOutlet weak var MoveUserInfo: UIButton!
    
    
    @IBAction func UpdateUserInfo(_ sender: Any) {
        
        //Show both of the Alerts
        getName()
        
    }
    
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var topbar: UIView!
    
    @IBOutlet weak var messageButtonLabel: UIButton!
    
    var scannedArray = [String:Double]()
    var scannedBlueBox = "";
    
    var sound: AVAudioPlayer?
    
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    
    
    func showPopOver(){
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if(Shared.shared.scannedMoveIpadArray.count == 0)
        {
            messageButtonLabel.setTitle("Scan the iPad you want to move.", for: .normal)
        }
        else
        {
            messageButtonLabel.setTitle("# of iPads to move: \(Shared.shared.scannedMoveIpadArray.count)", for: .normal)
            
        }
        
        /*   if(Shared.shared.scannedBlueBox.count == 0)
         {
         if(Shared.shared.scannedBlueBox == "")
         {
         messageButtonLabel.setTitle("Scan the iPads that have had content loaded", for: .normal)
         }
         else
         {
         messageButtonLabel.setTitle("# of iPads with new content: \(Shared.shared.scannedBlueBoxArray.count)", for: .normal)
         }
         
         }
         else
         {
         messageButtonLabel.setTitle("\(Shared.shared.scannedBlueBox ) has: \(Shared.shared.scannedBlueBoxArray.count) loaded", for: .normal)
         }
         
         */
        Shared.shared.uploadStoredData()
        
        if let date = UserDefaults.standard.object(forKey: "activeTime") as? Date {
            if let diff = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, diff > Shared.shared.timeOutTime {
                // Log the User out!
                DispatchQueue.main.async {
                    UserDefaults.standard.set(nil, forKey: "activeTime")
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
                let store = UserDefaults.standard
                let timeToStore = Date()
                store.set(timeToStore, forKey: "activeTime")
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("I am disapearing look at me!")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getName()
        //Shared.shared.resetDefaults()
        
        
        // messageButtonLabel.setTitle("Scan the iPads that have had content loaded", for: .normal)
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self as AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
        } catch {
            print(error)
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        captureSession.startRunning()
        
        
        view.bringSubviewToFront(MoveUserInfo)
        view.bringSubviewToFront(messageButtonLabel)
        view.bringSubviewToFront(topbar)
        
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        let alert = UIAlertController(title: "Notice", message: "Are you sure you want to cancel your current scan? Doing so will remove all active scans.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done, take me back home", style: UIAlertAction.Style.default, handler: { action in
            Shared.shared.scannedMoveIpadArray.removeAll()
                                       Shared.shared.scannedMoveIpad = ""
            self.cancelScan()
        }))
        alert.addAction(UIAlertAction(title: "OOPS! Didn't mean to do that", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelScan(){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
        
    }
    var name = "";
    var organization = "";
    
    
    func getName (){
        let alertController = UIAlertController(title: "Who is Receiving the iPad(s)?", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            
            if(self.name != "")
            {
                textField.text = self.name
            }
            else{
                textField.placeholder = "Name"
            }
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    self.name = textField.text!
                    //Organization the iPad is going to
                    self.getOrganization()
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func getOrganization(){
        let alertController = UIAlertController(title: "What Organization Do They Work For?", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            if(self.organization != "")
            {
                textField.text = self.organization
            }
            else{
                textField.placeholder = "Organization"
            }
            
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    self.organization = textField.text!
                    
                    
                    self.MoveUserInfo.setTitle("\(self.name) from \(self.organization)", for: .normal)
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.preferredAction = saveAction
        
        
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func save(_ sender: Any) {
        
        
        
        //Prompt User to enter
        
        //Name
        // getName()
        
        
        
        
        if(name != "" && organization != "")
        {
            //Save the data.
            let items = Shared.shared.scannedMoveIpadArray;
            if(items.count == 0)
            {
                let alert = UIAlertController(title: "Scan iPad", message: "You need to scan iPads before you can save.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let store = UserDefaults.standard
                let uuid = store.string(forKey: "uuid")
                
                let stuffToSend = [
                    "MoveiPad": [
                        "UUID":uuid!,
                        "iPads":items,
                        "TimeSaved": Date().timeIntervalSince1970,
                        "Name": name,
                        "Organization": organization
                        ] as [String:Any],
                ]
                
                
                /*     let stuffToSend = [
                 
                 "UUID":uuid!,
                 "iPads":items,
                 "TimeSaved": Date().timeIntervalSince1970,
                 "CarrierTimeStarted": Shared.shared.scannedBlueBoxStartTime
                 
                 ]  as [String : Any]
                 */
                let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                print(jsonString!)
                
                
                //if Reachability.isConnectedToNetwork(){
                let url = URL(string: "https://tracking.bulletdigital.com/move_ipad/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        print(responseJSON)
                        
                        let test = responseJSON["response_code"]
                        if(test as! Int == 200)
                        {
                            let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                self.sound = try AVAudioPlayer(contentsOf: url)
                                self.sound?.play()
                            } catch {
                                print("File not found!")
                            }
                            
                            //Clear out of the list
                            Shared.shared.scannedMoveIpadArray.removeAll()
                            Shared.shared.scannedMoveIpad = ""
                            
                            DispatchQueue.main.async {
                                //Go back to Home
                                let store = UserDefaults.standard
                                let timeToStore = Date()
                                store.set(timeToStore, forKey: "activeTime")
                                
                                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
                                viewController.modalPresentationStyle = .fullScreen
                                self.present(viewController, animated: true, completion: nil)
                            }
                            
                        }
                    }
                }
                task.resume()
                
                print("User is online so lets save that data!")
            }
            
            
        }
        else{
           print("No Matchy")
        }
        
    }
   
}

extension move_ipad: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let code = metadataObj.stringValue
                
                if code!.contains("CA"){
                    let alert = UIAlertController(title: "Scan iPad", message: "Please Scan an iPad", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("SP"){
                    Shared.shared.itemCurrentScanned = "MoveIpad"
                    
                    //  if Shared.shared.scannedBlueBox  != ""
                    // {
                    //Carrier has a value. Start loading iPads into the carrier.
                    
                    // let keyExsists = scannedArray[code!] != nil
                    
                    //if Shared.shared.dataToShow.contains(code!)
                    let keyExsists = Shared.shared.scannedMoveIpadArray[code!] != nil
                    if keyExsists
                    {
                        
                    }
                    else{
                        //Shared.shared.dataToShow.append(code!)
                        Shared.shared.scannedMoveIpadArray[code!] = Date().timeIntervalSince1970
                        
                        
                        let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            sound = try AVAudioPlayer(contentsOf: url)
                            sound?.play()
                        } catch {
                            // couldn't load file :(
                            print("File not found!")
                        }
                        self.saveBtn.backgroundColor = UIColor.green
                        self.saveBtn.blink()
                        messageButtonLabel.setTitle("# of iPads to move: \(Shared.shared.scannedMoveIpadArray.count)", for: .normal)
                        
                    }
                }
                    
                else if code!.contains("VE"){
                    let alert = UIAlertController(title: "Scan iPad", message: "Please Scan an iPad", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("AR"){
                    let alert = UIAlertController(title: "Scan iPad", message: "Please Scan an iPad", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func offlineUpload(){
        //Check if iPadToCarrier has values
        
    }
}
