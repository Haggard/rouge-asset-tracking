//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit

class home: UIViewController {
    
    
    @IBOutlet weak var unloadaircraft: UIButton!
    @IBOutlet weak var loadCarrier: UIButton!
    @IBOutlet weak var loadVehicle: UIButton!
    @IBOutlet weak var loadAircraft: UIButton!
    @IBOutlet weak var unloadCarrier: UIButton!
    @IBOutlet weak var unloadVehicle: UIButton!
    
    @IBAction func logOut(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes, Log out", style: UIAlertAction.Style.default, handler: { action in
            let store = UserDefaults.standard
            store.set("", forKey: "uuid")
            
            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*
         //loadCarrier.layer.cornerRadius = loadCarrier.frame.height / 2
         loadCarrier.layer.borderWidth = 1
         loadCarrier.layer.borderColor = UIColor.black.cgColor
         
         //loadVehicle.layer.cornerRadius = loadVehicle.frame.height / 2
         loadVehicle.layer.borderWidth = 1
         loadVehicle.layer.borderColor = UIColor.black.cgColor
         
         //loadAircraft.layer.cornerRadius = loadAircraft.frame.height / 2
         loadAircraft.layer.borderWidth = 1
         loadAircraft.layer.borderColor = UIColor.black.cgColor
         
         //unloadCarrier.layer.cornerRadius = unloadCarrier.frame.height / 2
         unloadCarrier.layer.borderWidth = 1
         unloadCarrier.layer.borderColor = UIColor.black.cgColor
         
         //unloadVehicle.layer.cornerRadius = unloadVehicle.frame.height / 2
         unloadVehicle.layer.borderWidth = 1
         unloadVehicle.layer.borderColor = UIColor.black.cgColor
         
         //unloadaircraft.layer.cornerRadius = unloadaircraft.frame.height / 2
         unloadaircraft.layer.borderWidth = 1
         unloadaircraft.layer.borderColor = UIColor.black.cgColor
         */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Shared.shared.uploadStoredData()
        
        if let date = UserDefaults.standard.object(forKey: "activeTime") as? Date {
            if let diff = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, diff > Shared.shared.timeOutTime {
                // Log the User out!
                
                DispatchQueue.main.async {
                    UserDefaults.standard.set(nil, forKey: "activeTime")
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
                let store = UserDefaults.standard
                let timeToStore = Date()
                store.set(timeToStore, forKey: "activeTime")
            }
        }
        
        
    }
    
}
