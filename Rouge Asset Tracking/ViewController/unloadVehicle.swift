//
//  ViewController.swift
//  Rouge Asset Tracking
//
//  Created by Dev Bullet on 2019-11-04.
//  Copyright © 2019 Bullet Digital. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation


class unloadVehicle: UIViewController {
    
    @IBOutlet weak var saveBtn: UIButton!
    
/*
     
     var scannedUnloadVehicle = "";
     var scannedUnloadVehicleStartTime = Double()
     var scannedUnloadVehicleArray = [String:Double]()
     
     
     */
    
    

    @IBOutlet weak var topbar: UIView!
    
    @IBOutlet weak var messageButtonLabel: UIButton!
    
    var scannedArray = [String:Double]()
    var scannedUnloadVehicle = "";
    
    var sound: AVAudioPlayer?
    
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    
    
    
    
    
    func showPopOver(){
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(Shared.shared.scannedUnloadVehicle.count == 0)
        {
            if(Shared.shared.scannedUnloadVehicle == "")
            {
                messageButtonLabel.setTitle("Scan The Vehicle You Want To Unload", for: .normal)
            }
            else
            {
                messageButtonLabel.setTitle("\(Shared.shared.scannedUnloadVehicle ) Ready To Unload", for: .normal)
            }
            
        }
        else
        {
            messageButtonLabel.setTitle("\(Shared.shared.scannedUnloadVehicle ) has: \(Shared.shared.scannedUnloadVehicleArray.count) unloaded", for: .normal)
        }
        Shared.shared.uploadStoredData()
        
        
        if let date = UserDefaults.standard.object(forKey: "activeTime") as? Date {
            if let diff = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, diff > Shared.shared.timeOutTime {
                // Log the User out!
                DispatchQueue.main.async {
                    UserDefaults.standard.set(nil, forKey: "activeTime")
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as UIViewController
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
              let store = UserDefaults.standard
              let timeToStore = Date()
              store.set(timeToStore, forKey: "activeTime")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("I am disapearing look at me!")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Shared.shared.resetDefaults()
        
       
        messageButtonLabel.setTitle("Scan The Vehicle You Want To Unload", for: .normal)
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self as AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
        } catch {
            print(error)
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        captureSession.startRunning()
        
        
        view.bringSubviewToFront(messageButtonLabel)
        view.bringSubviewToFront(topbar)
        
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        let alert = UIAlertController(title: "Notice", message: "Are you sure you want to cancel your current scan? Doing so will remove all active scans.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done, take me back home", style: UIAlertAction.Style.default, handler: { action in
            self.cancelScan()
        }))
        alert.addAction(UIAlertAction(title: "OOPS! Didn't mean to do that", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelScan(){
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "home") as UIViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    @IBAction func save(_ sender: Any) {
        let items = Shared.shared.scannedUnloadVehicleArray;
        let Vehicle = Shared.shared.scannedUnloadVehicle;
        
        if(items.count == 0 || Vehicle == "")
        {
            let alert = UIAlertController(title: "Scan Vehicle", message: "You need to load a Vehicle before you can save it. Please scan a Vehicle and then scan the Carriers as you unload them.", preferredStyle: UIAlertController.Style.alert)
         
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let store = UserDefaults.standard
            let uuid = store.string(forKey: "uuid")
            
            
            
            
        
          
            
            let stuffToSend = [
                Shared.shared.scannedUnloadVehicle: [
                    "UUID":uuid!,
                    "Carriers":items,
                    "TimeSaved": Date().timeIntervalSince1970,
                    "VehicleTimeStarted": Shared.shared.scannedUnloadVehicleStartTime
                    ] as [String:Any],
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
            
            
            if Reachability.isConnectedToNetwork(){
                let url = URL(string: "https://tracking.bulletdigital.com/unload_vehicle/?c668977d4ff12f105b1a5cccc1bb1b82")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                request.httpBody = jsonData
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        print(error?.localizedDescription ?? "No data")
                        return
                    }
                    print(data)
                    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        print(responseJSON)
                        
                        let test = responseJSON["response_code"]
                        if(test as! Int == 200)
                        {
                            let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                self.sound = try AVAudioPlayer(contentsOf: url)
                                self.sound?.play()
                            } catch {
                                print("File not found!")
                            }
                            
                            //Clear out of the list
                            Shared.shared.scannedUnloadVehicleArray.removeAll()
                            Shared.shared.scannedUnloadVehicle = ""
                            
                            DispatchQueue.main.async {
                                self.messageButtonLabel.setTitle("Scan The Vehicle You Want To Unload", for: .normal)
                            }
                            
                        }
                    }
                }
                task.resume()
            }else{
                print("Internet Connection not Available!")
                print("User is offline so lets save that data somewhere to send later!")
                
                //CHECK IF THERE IS ANYTHING IN VehicleToVehicle  USER DEFUALT STORAGE
                let store = UserDefaults.standard
                let CarrierToVehicle = store.string(forKey: "CarrierToVehicle")
                
                if(CarrierToVehicle != nil && CarrierToVehicle != "" && CarrierToVehicle != "{:}"){
                    let data = Data(CarrierToVehicle!.utf8)
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            print(json)
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
         
                    // KEEP CarrierToVehicle as the main, remove the first and last,
                    let modIpad = CarrierToVehicle?.dropLast(1)
                    let modScanned = jsonString?.dropFirst(1)
                    let jsonString123 = modIpad! + "," + modScanned!
                    print(jsonString123)
                    store.set(jsonString123,forKey: "CarrierToVehicle")
                    
                    let path = Bundle.main.path(forResource: "Chord.mp3", ofType:nil)!
                    let url = URL(fileURLWithPath: path)
                    do {
                        self.sound = try AVAudioPlayer(contentsOf: url)
                        self.sound?.play()
                    } catch {
                        // couldn't load file :(
                        print("File not found!")
                    }
                    
                    Shared.shared.scannedUnloadVehicle = ""
                    Shared.shared.scannedUnloadVehicleArray.removeAll()
                    messageButtonLabel.setTitle("Scan The Vehicle You Want To Unload", for: .normal)
                }
                else{
                    //FALSE--- ADD THE NEW STUFF TO USER DEFAULT STORAGE
                    let jsonData = try? JSONSerialization.data(withJSONObject: stuffToSend, options: [])
                    let jsonString = String(data: jsonData!, encoding: .utf8)
                    
                    store.set(jsonString,forKey: "CarrierToVehicle")
                    
                    Shared.shared.scannedUnloadVehicle = ""
                    Shared.shared.scannedUnloadVehicleArray.removeAll()
                    messageButtonLabel.setTitle("Scan The Vehicle You Want To Unload", for: .normal)
                }
                self.saveBtn.blink(enabled:false)
                self.saveBtn.backgroundColor = UIColor.clear
                return
            }
            print("User is online so lets save that data!")
        }
    }
}

extension unloadVehicle: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let code = metadataObj.stringValue
                
                if code!.contains("VE"){
                    print("Vehicle Scanned");
                    
                    if Shared.shared.scannedUnloadVehicle == code!{
                        
                    }
                    else if( Shared.shared.scannedUnloadVehicle != "")
                    {
                        
                      
                        
                        let path = Bundle.main.path(forResource: "error.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            sound = try AVAudioPlayer(contentsOf: url)
                            sound?.play()
                        } catch {
                            // couldn't load file :(
                            print("File not found!")
                        }
                        
                        
                        //Show alert saying that a Vehicle has already been scanned
                        let alert = UIAlertController(title: "Duplicate Vehicle", message: "You have already scanned a Vehicle with the tag: \(Shared.shared.scannedUnloadVehicle). Starting a new Vehicle will erease all the iPads scanned to the old Vehicle", preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction(title: "Start Loading The New \(code!) Vehicle", style: UIAlertAction.Style.default, handler: { action in
                            //print("Fuck, now I have to handle if they want to load a new Vehicle")
                            
                            //Clear out of the list
                            //  Shared.shared.dataToShow.removeAll()
                            Shared.shared.scannedUnloadVehicle = code!
                            Shared.shared.scannedUnloadVehicleStartTime = Date().timeIntervalSince1970;
                            Shared.shared.scannedUnloadVehicleArray.removeAll()
                            
                            DispatchQueue.main.async {
                                //messageButtonLabel.setTitle("\(Shared.shared.scannedUnloadVehicle ) Ready To Unload", for: .normal)
                                self.messageButtonLabel.setTitle("\(code! ) Ready To Unload", for: .normal)
                            }
                            
                            //Wipe out eve
                        }))
                        alert.addAction(UIAlertAction(title: "OOPS! Keep loading current Vehicle", style: UIAlertAction.Style.cancel, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        //Scanned Carrier is blank.
                        //Update scannedUnloadVehicle value to the one scanned.
                          Shared.shared.itemCurrentScanned = "UnloadVehicle"
                        Shared.shared.scannedUnloadVehicle  = code!
                        Shared.shared.scannedUnloadVehicleStartTime = Date().timeIntervalSince1970;
                        
                        let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        
                        do {
                            sound = try AVAudioPlayer(contentsOf: url)
                            sound?.play()
                        } catch {
                            // couldn't load file :(
                            print("File not found!")
                        }
                       
                        messageButtonLabel.setTitle("\(Shared.shared.scannedUnloadVehicle ) Ready To Unload", for: .normal)
                        
                    }
                }
                else if code!.contains("CA"){
                    if Shared.shared.scannedUnloadVehicle  != ""
                    {
                        //Carrier has a value. Start loading iPads into the carrier.
                        
                        // let keyExsists = scannedArray[code!] != nil
                        
                        //if Shared.shared.dataToShow.contains(code!)
                        let keyExsists = Shared.shared.scannedUnloadVehicleArray[code!] != nil
                        if keyExsists
                        {
                            
                        }
                        else{
                            //Shared.shared.dataToShow.append(code!)
                            Shared.shared.scannedUnloadVehicleArray[code!] = Date().timeIntervalSince1970
                            
                            
                            let path = Bundle.main.path(forResource: "iphone-ding-sound.mp3", ofType:nil)!
                            let url = URL(fileURLWithPath: path)
                            
                            do {
                                sound = try AVAudioPlayer(contentsOf: url)
                                sound?.play()
                            } catch {
                                // couldn't load file :(
                                print("File not found!")
                            }
                            self.saveBtn.backgroundColor = UIColor.green
                                                       self.saveBtn.blink()
                            messageButtonLabel.setTitle("\(Shared.shared.scannedUnloadVehicle ) has: \(Shared.shared.scannedUnloadVehicleArray.count) unloaded", for: .normal)
                            
                        }
                        
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Scan Vehicle", message: "You need to scan a Vehicle before you can scan an Carrier", preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else if code!.contains("SP"){
                    let alert = UIAlertController(title: "Scan Vehicle or Carrier", message: "Please Scan a Vehicle or Carrier", preferredStyle: UIAlertController.Style.alert)
                                                                                              
                                                                                              // add the actions (buttons)
                                                                                              alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                                                                              
                                                                                              // show the alert
                                                                                              self.present(alert, animated: true, completion: nil)
                }
                else if code!.contains("AR"){
                    let alert = UIAlertController(title: "Scan Vehicle or Carrier", message: "Please Scan an Vehicle or Carrier", preferredStyle: UIAlertController.Style.alert)
                                                                                              
                                                                                              // add the actions (buttons)
                                                                                              alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                                                                                              
                                                                                              // show the alert
                                                                                              self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func offlineUpload(){
        //Check if CarrierToVehicle has values
        
    }
}
